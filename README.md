# Knowledge Distillation

Incipient project which explores knowledge distillation in 3 different stages.
* 1st stage involves simple teacher, student architecture trained on a simple dataset [Fashion MNIST](https://github.com/zalandoresearch/fashion-mnist). This is to establish the entire pipeline.
* 2nd stage uses a pre-trained fully supervised teacher and we train our own student and baseline. Dataset used is [CIFAR-10](https://www.cs.toronto.edu/~kriz/cifar.html).
* 3rd stage use pre-trained self-supervised (SS) model as teacher to train student and baseline. For the SS model experiment with [resource 1](https://arxiv.org/abs/1905.01235) various datasets.

### Prerequisites

Jupyter notebook

